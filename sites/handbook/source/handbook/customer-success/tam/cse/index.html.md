---
layout: handbook-page-toc
title: "Customer Success Engineer Handbook"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ This information is under active development and is not yet final. This page will be updated on an ongoing basis during this phase.
{: .alert .alert-warning}

## CSE Mission
The Customer Success Engineer (CSE) provides deep subject matter expertise on GitLab technical and product solutions and best practices. CSEs work alongside CSMs to provide customers with technical guidance, go in-depth on use case implementation, and demonstrate the value of GitLab product capabilities.

## Role & Responsibilities
Engage with customers in a technical consultancy and advisor role during their GitLab Journey while providing technical and solution guidance. This is achieved by driving towards measurable value (business outcomes), leading to product adoption, renewal, and expansion opportunities. The team is focused on solution-based programs that are customized to fit an individual customer's needs. 

## GitLab Use Cases supported by CSEs

⚠️ The CSE role is in active development, so not all use cases may be currently supported. Please check with CSM/CSE leadership for your territory on coverage & availability.

CSEs will support the following GitLab use cases:

- Agile Planning
- SCM
- CI
- CD
- DevSecOps
- DevOps Platform Scaling
- API usage and Automation
- Value Stream Assessment
- CSM Office Hours

## Deliverables & Scope per Use Case
- CSE is working on a fixed (time), scoped (deliverables) engagement with a dedicated area/business unit/team (aligned) within an existing customer.
- CSE engagement is supporting adoption and growth within a GitLab Use Case.
- Deliverables are defined upfront and mutually agreed between Customer, CSM, CSE, SA.
- Deliverables are templated, success metrics can vary based on customer situation.
- Results and adoption patterns are documented in the handbook or Docs.

## Engagement Model

Engagement Criteria:

| Engagement Criteria                                                                    | mandatory/optional |
|----------------------------------------------------------------------------------------|--------------------|
| Agreed Success Plan with measurable Business Outcomes aligned to the CSE deliverables  | mandatory          |
| Exec Alignment and access to impacted customer teams                                   | mandatory          |
| Included in Account Plan                                                               | mandatory          |

Engagement Qualification:
1. Identification of CSE deliverable - link issue template (DRI: CSM)
1. Alignment Account Team (DRI: CSM)
1. Review and Approval (DRI: Manager, CSM) 
1. Capacity Planning (DRI: Manager, CSE)

## Metrics & Key Performance Indicators
1. 100% adoption & growth per defined use case and metric
1. 80% achievement of deliverable within defined timeline
1. NPS/CSAT Post-CSE Engagement Survey
1. Updated Docs and Guidelines (1 per customer engagement)

## Hiring Process

## CSE Hiring Plan

The CSE hiring plan follows [much of the same process as the TAM/CSM](/job-families/sales/technical-account-manager/#hiring-process), with [the exception of a technical panel](https://sa-demo-group.gitlab.io/sa-candidate-experience/panel_instructions) that evaluates the candidate's technical aptitude along with presentation style, clarity of the GitLab value proposition and audience engagement.

The hiring plan is as follows:
1. Recruiter call (external candidates only)
1. Hiring manager call
1. Peer call 
1. Panel 
1. Senior CS leadership call
